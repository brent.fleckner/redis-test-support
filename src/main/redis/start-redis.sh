#!/usr/bin/env sh

# Usage is via env vars:
#   REDIS_TEST_SUPPORT_TAG: docker image tag to use, default "latest"
#   REDIS_TEST_SUPPORT_PORT: visible port to map to container port, default is content of file default-redis-test-port
#   REDIS_TEST_SUPPORT_CONTAINER: name of container, default is content of file default-redis-test-container
#   REDIS_TEST_SUPPORT_CONTAINER_PORT: redis client port in container, default 6379
#   REDIS_TEST_SUPPORT_IMAGE: docker image name, default "redis"

THIS_DIR="$(cd "$(dirname "$0")"; pwd)"

if [ -n "$CI" ]; then # we're in CI pipeline & not forcing start
  echo 'in CI pipeline; container is assumed to be started'
  exit 0
fi

if [ -z "$REDIS_TEST_SUPPORT_CONTAINER" ]; then
  REDIS_TEST_SUPPORT_CONTAINER="$(cat $THIS_DIR/default-redis-test-container)"
fi

if [ -z "$REDIS_TEST_SUPPORT_REDIS_PORT" ]; then
  REDIS_TEST_SUPPORT_REDIS_PORT="$(cat $THIS_DIR/default-redis-test-port)"
fi

if [ -z "$(docker ps --quiet --filter name=$REDIS_TEST_SUPPORT_CONTAINER)" ]; then
  "$THIS_DIR/start-redis-container.sh"
fi
