#!/usr/bin/env sh

# Usage is via env vars:
#   REDIS_TEST_SUPPORT_TAG: docker image tag to use, default "latest"
#   REDIS_TEST_SUPPORT_PORT: visible port to map to container port, default is content of file default-redis-test-port
#   REDIS_TEST_SUPPORT_CONTAINER: name of container, default is content of file default-redis-test-container
#   REDIS_TEST_SUPPORT_CONTAINER_PORT: redis client port in container, default 6379
#   REDIS_TEST_SUPPORT_IMAGE: docker image name, default "redis"

THIS_DIR="$(cd "$(dirname "$0")"; pwd)"

REDIS_TEST_SUPPORT_TAG=${REDIS_TEST_SUPPORT_TAG:-latest}
REDIS_TEST_SUPPORT_CONTAINER_PORT=${REDIS_TEST_SUPPORT_CONTAINER_PORT:-6379}
REDIS_TEST_SUPPORT_CONTAINER_IMAGE=${REDIS_TEST_SUPPORT_CONTAINER_IMAGE:-redis}

if [ -z "$REDIS_TEST_SUPPORT_CONTAINER" ]; then
  REDIS_TEST_SUPPORT_CONTAINER="$(cat $THIS_DIR/default-redis-test-container)"
fi

if [ -z "$REDIS_TEST_SUPPORT_PORT" ]; then
  REDIS_TEST_SUPPORT_PORT="$(cat $THIS_DIR/default-redis-test-port)"
fi

RUNNING=$(docker inspect --format="{{ .State.Running }}" "$REDIS_TEST_SUPPORT_CONTAINER" 2> /dev/null)

if [ "$RUNNING" == "true" ]; then
  exit 0
fi

# else container is stopped or unknown - forcefully recreate
echo "container '$REDIS_TEST_SUPPORT_CONTAINER' is stopped or unknown - recreating"

# make sure it's gone
docker ps -a | \
  grep "$REDIS_TEST_SUPPORT_CONTAINER" | \
  awk '{ print $1}' | \
  xargs docker rm --force

CMD="docker run --name $REDIS_TEST_SUPPORT_CONTAINER -p $REDIS_TEST_SUPPORT_PORT:$REDIS_TEST_SUPPORT_CONTAINER_PORT -d $REDIS_TEST_SUPPORT_CONTAINER_IMAGE:$REDIS_TEST_SUPPORT_TAG"
echo "$CMD"
$CMD
